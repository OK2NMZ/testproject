[[_TOC_]]

# Lua
## Introduction
This section is divided in three parts, the first describes the Lua API functions exposed by the device software in order to implement the final device application logic (referred to often as measurement sequence). The seconds describes the Lua events, or Lua callbacks, which allow the Lua API user to implement his own handling of important application events using Lua API functions and generic Lua code. The third part describes the current Lua script used for implementation of the device application logic.

## Lua API
The Lua API consist of a set of approximately 50 functions allowing to define the device software application logic. The functions can be divided in different categories - commands, getters, setters, memory operations, boolean getters and internal functions.

### Commands

#### hardReset()

This function causes the device to execute a hardware reset.

**Arguments**
  * none

**Returns**
  * none

---

#### *result* = selfTest()

This function executes the device self-test and returns the result.

**Arguments**
  * none

**Returns**
  * *result* (integer) - zero on success, positive on error (bit flag array)

---

#### statusLED(*state*)

This function controls the built-in status LED to visually communicate with the user.

**Arguments**
  * *state* (string) - new state of the status LED as string.
      * "red"/"RED" for red color
      * "green"/"GREEN" for green color
      * "blue"/"BLUE" for blue color
      * "off"/"OFF"/other for LED off state

**Returns**
  * none

---

#### illuminationLED(*greenState*, *blueState*)

This function controls the LEDs used for illumination. Each color channel is controlled independently.

**Arguments**
  * *greenState* (integer) - PWM value in percentage for green channel/LED, ranging 0-100
  * *blueState* (integer) - PWM value in percentage for blue channel/LED, ranging 0-100

**Returns**
  * none

---

#### *status* = sendReport(*deviceStatus*, *testStatus*, *secondsLeft*, *testID*)

This function sends current state of the deviceInfo internal structure, optionally updating it with provided values.

**Arguments**
  * *deviceStatus* (string) [**optional**] - new device status, on unknown state provided, error state is used.
      * "IDLE" - idle state of the device
      * "PERFORMING_SELF_CHECK" - device is performing self-check
      * "SELF_CHECK_PASSED" - self-check passed state
      * "SELF_CHECK_FAILED" - self-check failed state
      * "PREHEATING" - device is in preheating state
      * "READY_FOR_TEST" - device is ready for test start
      * "SAMPLE_DETECTED" - device detected the sample
      * "RUNNING_TEST" - device is performing a test
      * "FINISHED_TEST" - device finished the test
      * "ERROR" - device is in error state
      * "CONFIGURATION_SUCCESSFUL" - device configuration was successfully uploaded and applied
      * "CONFIGURATION_FAILED" - device failed to get new configuration or invalid Lua script provided
  * *testStatus* (string) [**optional**] - new test status, on unknown state provided, aborted state is used.
      * "NEW" - the test is new, not yet executed
      * "RUNNING" - the test is being executed
      * "POSITIVE" - the test confirms virus presence
      * "NEGATIVE" - the test confirms no virus found
      * "INCONCLUSIVE" - the test cannot conclude on positive or negative result
      * "ABORTED" - the test was aborted, stopped and to be discarded
  * *secondsLeft* (integer)  [**optional**] - seconds left until the test is done
  * *testID* (string) [**optional**] - new test ID as string

**Returns**
  * *status* (integer) - zero on success, negative on error

---

#### *status* = addLog(*entryType*, *timestamp*, ...)

This function adds entry in the outbound buffer, which can be later sent to the BLE-connected device by calling *sendReport()*. The entries log can be erased using *clearLog()*.

**Arguments**
  * *entryType* (string) - type of the entry, defining the function prototype to use (polymorphic function)
      * "TEMPERATURE" - only the temperatures are logged
          * prototype used: **addLog**(*entryType*, *timestamp*, *temperatures*)
          * *temperatures* (float array) - array of temperature entries to log
      * "SPEC_ANALYSIS" - only the spectral analysis data are logged
          * prototype used: **addLog**(*entryType*, *timestamp*, *spectrum*)
          * *spectrum* (float array) - array of spectral bands power entries to log
      * "BOTH" - both spectrum and temperatures are logged
          * prototype used: **addLog**(*entryType*, *timestamp*, *spectrum*, *temperatures*)
          * *spectrum* (float array) - array of spectral bands power entries to log
          * *temperatures* (float array) - array of temperature entries to log
      * "MESSAGE" - only a text message with timestamp is logged
          * prototype used: **addLog**(*entryType*, *timestamp*, *message*)
          * *message* (string) - custom text message, allows custom message to be sent through
  * *timestamp* (integer) - integer timestamp, refer to *getTick()* to get a ms timestamp

**Returns**
  * *status* (integer) - zero on success, negative on error

---

#### clearLog()

This function disposes all log entries added by *addLog()* function.

**Arguments**
  * none

**Returns**
  * none
---

#### wipeData()

This function invalidates/deletes the content of the samples flash memory built using *saveEntry()* function and accessible using *loadEntry()* function.
It also resets the DeviceInfo internal structure to the default start-up state.

**Arguments**
  * none

**Returns**
  * none
---

#### delayms(*ms*)

This function blocks the execution for provided number of miliseconds.

**Arguments**
  *ms* (integer) - number of ms to wait/delay/block

**Returns**
  * none
---

#### chargerEnable(*newState*)

This function enables the charging if *newState* is non-zero or true, disables otherwise.

**Arguments**
  *newState* (integer) - new state of the charger

**Returns**
  * none
---

### Getters

#### *volts* = getBatteryVoltage()

This function returns the current battery voltage in volts.

**Arguments**
  * none

**Returns**
  * *volts* (float) - current battery voltage in volts as floating point number

---

#### *version* = getBoardVersion()

This function returns the board version as integer number.

**Arguments**
  * none

**Returns**
  * *version* (integer) - board version as integer

---

#### *UUID* = getBoardUUID()

This function returns the board unique identifier UUID as string.

**Arguments**
  * none

**Returns**
  * *UUID* (string) - board UUID

---

#### *ID* = getFlashID()

This function returns the external flash ID number.

**Arguments**
  * none

**Returns**
  * *ID* (integer) - external flash ID

---

#### *celsius* = getSystemTemperature()

This function returns current system temperature in degrees of Celsius

**Arguments**
  * none

**Returns**
  * *celsius* (float) - current system temperature in degrees of Celsius

---

#### *celsius* = getSampleTemperature(*sensorNumber*)

This function returns current sample temperature from sensor number provided in degrees of Celsius.

**Arguments**
  * *sensorNumber* (integer) - sensor to use (1 or 2), *celsius* return value is "nil" if bad sensor number is provided

**Returns**
  * *celsius* (float) - current temperature from selected sensor in degrees of Celsius.

---


#### *celsius* = getCPUTemperature()

This function returns current CPU temperature from internal CPU sensor in degrees of Celsius.

**Arguments**
  * none

**Returns**
  * *celsius* (float) - current CPU temperature in degrees of Celsius

---

#### *spectrum*, *dataValid*, *gainApplied*, *saturationDetected* = getSpectralData(*channels*, *timeout*)

This function returns current spectral data from the spectrometer, optionally a *timeout* can be specified for communication with the spectrometer. The spectral channels/bins to measure are defined in *channels* argument.

**Arguments**
  * *channels* (integer array) - list of channels to measure, up to 6 channels ranging from 1 to 6.
  * *timeout* (integer) [**optional**] - maximum execution time when communicating with the spectrometer, can be omitted - in such case the function blocks until the spectral data are provided by the spectrometer.

**Returns**
  * *spectrum* (float array) - relative power for specified spectral channels, sorted in the same order as specified in *channels* argument
  * *dataValid* (integer) - if set to 1, the provided data are valid, if 0, the measured data are invalid (reported by the spectrometer)
  * *gainApplied* (integer) - if 1, a gain was applied, if 0, gain was not applied during measurement
  * *saturationDetected* (integer) - if 1, the sensor is in saturation (no sample inserted?), if 0, then no saturation detected

---

#### *timestamp* = getTick()

This function returns current value of the system milisecond tick timer.

**Arguments**
  * none

**Returns**
  * *timestamp* (integer) - current value of the 32-bit milisecond system tick timer

---

#### *length* = getEntriesLength()

This function returns current number of samples saved using *saveEntry()* function since startup or last call of *wipeData()* function.

**Arguments**
  * none

**Returns**
  * *length* (integer) - current length/depth of the samples memory

---

#### *mode* = getChargerMode()

This function returns current charging mode of the charger.

**Arguments**
  * none

**Returns**
  * *mode* (integer) - current charging mode
      * 1 - quick charging mode
      * 0 - normal charging mode
      * -1 - charging error

---

### Setters


#### setHeaterTemperature(*offTemperature*, *onTemperature*)

This function is used to set target temperature for the internal heater. If only *offTemperature* is provided, then no hysteresis is applied in the regulation process. If both *onTemperature* and *offTemperature* are provided, then the regulation has hysteresis. Then *offTemperature* is reached, the heater is turned off and when (during "cooling period") the *onTemperature* is reached, the heater is turned on again. (therefore use *onTemperature* < *offTemperature*)

**Arguments**
  * *offTemperature* (float) - setpoint temperature in degrees of Celsius
  * *offTemperature* (float) [**optional**] - setpoint temperature in degrees of Celsius, used for hysteresis (refer to the description above)

**Returns**
  * none
---

#### setDeviceStatus(*deviceStatus*)

This function sets new device status, for reporting the change, use *sendReport()* function.

**Arguments**
  * *deviceStatus* (string) - new device status, on unknown state provided, error state is used.
      * "IDLE" - idle state of the device
      * "PERFORMING_SELF_CHECK" - device is performing self-check
      * "SELF_CHECK_PASSED" - self-check passed state
      * "SELF_CHECK_FAILED" - self-check failed state
      * "PREHEATING" - device is in preheating state
      * "READY_FOR_TEST" - device is ready for test start
      * "SAMPLE_DETECTED" - device detected the sample
      * "RUNNING_TEST" - device is performing a test
      * "FINISHED_TEST" - device finished the test
      * "ERROR" - device is in error state
      * "CONFIGURATION_SUCCESSFUL" - device configuration was successfully uploaded and applied
      * "CONFIGURATION_FAILED" - device failed to get new configuration or invalid Lua script provided
**Returns**
  * none
---

#### setTestStatus(*deviceStatus*)

This function sets new device status, for reporting the change, use *sendReport()* function.

**Arguments**
  * *deviceStatus* (string) - new device status, on unknown state provided, error state is used.
      * "IDLE" - idle state of the device
      * "PERFORMING_SELF_CHECK" - device is performing self-check
      * "SELF_CHECK_PASSED" - self-check passed state
      * "SELF_CHECK_FAILED" - self-check failed state
      * "PREHEATING" - device is in preheating state
      * "READY_FOR_TEST" - device is ready for test start
      * "SAMPLE_DETECTED" - device detected the sample
      * "RUNNING_TEST" - device is performing a test
      * "FINISHED_TEST" - device finished the test
      * "ERROR" - device is in error state
      * "CONFIGURATION_SUCCESSFUL" - device configuration was successfully uploaded and applied
      * "CONFIGURATION_FAILED" - device failed to get new configuration or invalid Lua script provided

**Returns**
  * none
---

#### setTestStatus(*deviceStatus*)

This function sets new test status, for reporting the change, use *sendReport()* function.

**Arguments**
 * *testStatus* (string) - new test status, on unknown state provided, aborted state is used.
      * "NEW" - the test is new, not yet executed
      * "RUNNING" - the test is being executed
      * "POSITIVE" - the test confirms virus presence
      * "NEGATIVE" - the test confirms no virus found
      * "INCONCLUSIVE" - the test cannot conclude on positive or negative result
      * "ABORTED" - the test was aborted, stopped and to be discarded
      
**Returns**
  * none
---

#### setSecondsLeft(*secondsLeft*)

This function sets new value of remaining seconds, for reporting the change, use *sendReport()* function.

**Arguments**
* *secondsLeft* (integer)  - seconds left until the test is done
      
**Returns**
  * none
---

#### setTestId(*testID*)

This function sets new test ID, for reporting the change, use *sendReport()* function.

**Arguments**
  * *testID* (string) - new test ID as string
      
**Returns**
  * none
---

#### setDeviceId(*deviceID*)

This function sets new device ID, for reporting the change, use *sendReport()* function.

**Arguments**
  * *deviceID* (string) - new device ID as string
      
**Returns**
  * none
---

#### setVibrationMotor(*percent*)

This function sets vibration motor intensity in percent.

**Arguments**
  * *percent* (integer) - vibration motor intensity in percent (0-100%)
      
**Returns**
  * none
---

#### setPrintState(*state*)

This function controls verbosity of device console printing.

**Arguments**
  * *state* (integer) - zero disables debug printing, non-zero value enables it.
      
**Returns**
  * none
---

### Sample Saving Operations


#### *status* = saveEntry(*index*, *timestamp*, *temperatures*, *spectrum*, *message*)

This function saves provided data to external flash memory. The first argument *index* is optional and can be omitted.

**Arguments**
  * *index* (integer) [**optional**] - index to memory where to save the data
  * *timestamp* (integer) - timestamp in miliseconds of saved sample
  * *temperatures* (float array) - array of temperature to save
  * *spectrum* (float array) - array of spectral data to save
  * *messenger* (string) [**optional**] - text message to save with the sample

**Returns**
  * *status* (integer) - zero on success, negative on error

---

#### *timestamp*, *temperatures*, *spectrum*, *message* = loadEntry(*index*)

This function saves provided data to external flash memory. The first argument *index* is optional and can be omitted.

**Arguments**
  * *index* (integer) - index to memory where to read from

**Returns**
  * *timestamp* (integer) - timestamp in miliseconds of loaded sample
  * *temperatures* (float array) - loaded array of temperature value
  * *spectrum* (float array) - loaded array of spectral data
  * *messenger* (string) - loaded text message or empty string
---

### Boolean Getters


#### *value* = isChargerConnected()

This function provides charger connection status.

**Arguments**
  * none

**Returns**
  * *value* (integer) - current charging mode
      * 1 - charger is connected
      * 0 - charger is disconnected
---

#### *value* = isChargerActive()

This function provides charger activity status - charging or not chaging.

**Arguments**
  * none

**Returns**
  * *value* (integer) - current charging activity
      * 1 - charger is charging
      * 0 - charger is not charging
---

#### *value* = isChargerEnabled()

This function provides information on charger enabled/disabled status.

**Arguments**
  * none

**Returns**
  * *value* (integer) - charger enabled/disabled status
      * 1 - charger is enabled
      * 0 - charger is disabled
---

#### *value* = isSetpointReached()

This function provides information on heater reaching current setpoint temperature. Every time the setpoint temperature is changed using *setHeaterTemperature()*, then this is re-evaluated internally. Once the setpoint is reached, this flag is set to true and latched until the next change of by calling *setHeaterTemperature()*.

**Arguments**
  * none

**Returns**
  * *value* (integer) - setpoint reached status
      * 1 - setpoint has been reached
      * 0 - setpoint not yet reached
---

### Internal/Reserved

#### *receiveBuffer* = _i2cTransfer(*slaveAddress*, *transmitBuffer*, *expectedReceiveSize*)

This function provides access to the internal I2C bus - this allows e.g. direct access to register values of connected ICs.

**Arguments**
  * *slaveAddress* (integer) - I2C address of the slave device
  * *transmitBuffer* (string) - buffer of bytes to send to the I2C slave device
  * *expectedReceiveSize* (integer) - expected number of bytes received from the slave device back

**Returns**
  * *receiveBuffer* (string) - bytes received from the slave device
---

## Lua Events

The Lua events are callbacks provided by the device software, which are implemented in Lua. When creating the device software Lua script to define the application-level logic, these callbacks should be used to define the behavior of the device. The context created by all functions is preserved between different callbacks.

### preheat(*testId*)
This callback is called, when the peer connected over BLE requests the test sample to be preheated. When returning execution from this callback, the sample should have reached the requested temperature.

**Arguments**
  * *testId* (string) - ID of current test

### startTest(*testId*)
This callback starts the actual test. The application logic programmer should control the illumination LEDs, change heater temperature if needed, sample temperature sensors' value, read spectrometer reported spectrum and save it using the *saveEntry()* function for later usage and transfer. Once the *startTest()* callback returns, the firmware disables the heater, turns off illumination LEDs, etc.

**Arguments**
  * *testId* (string) - ID of current test


### getTestResult()
In this callback, the user can process the data by loading it using *loadEntry()* function sample by sample and applying any integration, window functions or any signal/data processing algorithms required to get the result of the test. *setTestStatus()* function is then used to set the result of the test.

**Arguments**
  * none

### getMeasurementData(offset, length)
This callback is called when the peer connecter over BLE asks for the samples. *loadEntry()* should be used to load the data from the flash memory and *addLog()* function should be used to build a batch of samples to upload using the *sendReport()* function.

**Arguments**
  * *offset* (integer) - offset from the start of samples memory to read from
  * *length* (integer) - log length requested to be sent using the *sendReport()* function

### wipe()
This callback is used to invalidate/destroy/remove any test/measurement data gathered during the measurement. Call explicitly the *wipeData()* function to execute this.

**Arguments**
  * none

## Current Lua script

The latest Lua script used in development is located here: https://git.midgemedical.de/firmware/blue/-/blob/master/blue/measurement_sequence.lua

Once development is freezed, detailed description of the algorithm is to be provided here. [TODO] 
